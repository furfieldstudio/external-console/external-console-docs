# Roadmap

## External Console
### 1.2 - Multiple connections
(This update will also bump C# Log Servers version)
- Add support for being connected to multiple Log Servers

## Log Server Unity/C&#35;
### 1.1 - Keep it alive
- (Unity) Adding support for editor runtime so users don't need to constantly connect with Log Server.
- (C#) Add support for normal console.
- (C#) Add support for Log4Net.

### 2.0 - Mmmmm console variables
- (Unity/C#) Add console variables (cvars) system.