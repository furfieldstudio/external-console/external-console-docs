# Commands

Commands are separated into two types:

- runtime commands
- internal console commands

## Runtime commands
Runtime commands are commands that are implemented by a developer in the project.
<br>
The only build-in command is ``Help`` or ``help`` for getting all commands.

### Adding new commands
To add a new command add ``[ConsoleCommand({command},{Description})]`` attribute to a method.
You can also add ``[Conditional("ENABLE_COMMANDS")]`` to support stripping the methods used for commands.

Example:
```csharp
    [Conditional("ENABLE_COMMANDS")]
    [ConsoleCommand(nameof(DebugLogTest) + ".LogError","Logs test error")]
    public void LogError()
    {
        Debug.LogError("error");
    }
```

We recommend writing commands using this pattern ``{ClassName}.{MethodName}``.
<br>
ExternalConsole also support arguments in commands. Everything after command that is separated by space will be placed in array and used to as input for method.
The example 

## Internal console commands
Internal console commands are commands implemented in External Console, they mostly start with ``console`` and are separated by space

The list of internal console commands is available here:

### Connect
``console connect {CommandsClientPort} {LogServerPort}``
<br>
Connects to Log Server

### Disconnect
``console disconnect``
<br>
Disconnects from Log Server

### Disconnect
``console exit``
<br>
Closes External Console