# Installation

External Console is general purpose debugging console that allow you to remotely connect to any software or game that has our dll server bundled.

Currently, External Console is available for: Unity, C#

## Unity

- Import the ExternalConsole.unitypackage into the project.
- Add ExternalConsoleManager script to any GameObject and configure Log Server and Commands Client ports.
- Now we need to run proper ExternalConsole.
- Go to Assets/FurFieldStudio/ExternalConsole and unzip ExternalConsole.zip somewhere on your computer.
In the folder there is ExternalConsole.exe, which is our console executable used to connect to Log Server and send commands to it.

## C&#35;
//TODO